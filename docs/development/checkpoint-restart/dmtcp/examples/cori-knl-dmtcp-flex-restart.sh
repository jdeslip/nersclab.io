#!/bin/bash
#SBATCH -J test
#SBATCH -q flex
#SBATCH -N 1 
#SBATCH -C knl
#SBATCH -t 48:00:00
#SBATCH -o test_cr-%j.out
#SBATCH -e test_cr-%j.err
#SBATCH --time-min=2:00:00

#for c/r with dmtcp
module load dmtcp nersc_cr

#checkpointing once every hour
start_coordinator -i 3600

#restarting from dmtcp checkpoint files
./dmtcp_restart_script.sh 


