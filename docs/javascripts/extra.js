window._monsido = window._monsido || {
    token: "hmGcbrWIxrBfnWAdggpDSg",
    statistics: {
        enabled: true,
        documentTracking: {
            enabled: true,
            documentCls: "monsido_download",
            documentIgnoreCls: "monsido_ignore_download",
            documentExt: [],
        },
    },
    pageCorrect: {
        enabled: true,
    },
    pageAssist: {
        enabled: true,
        theme: "dark",
        direction: "right",
        skipTo: true,
    },
};
