# Programming Environment Change on Cori in July 2019

## Background

The operating system (OS) on Cori will be upgraded from CLE6.0UP07 to
CLE7.0UP00, which is a major version upgrade that includes upgrading
the kernel from SLES12 to SLES15. Due to the change in the operating
system, Cray software versions older than
[CDT/19.03](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_19.03_March_7_2019/resource/Released_Cray_XC_Programming_Environments_19.03_March_7_2019.pdf)
no longer work. Thus, **rebuilding your applications is
required**. NERSC is also preparing to rebuild the software we support
so that the most important software for users is available after the
OS upgrade.

After the July 26-30 power outage and Cori maintenance, new default
software environments will be set. Highlights of the changes include
setting the Intel compiler default version to 19.0.3.199; using the
Cray software environment versions in
[CDT/19.03](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_19.03_March_7_2019/resource/Released_Cray_XC_Programming_Environments_19.03_March_7_2019.pdf)
as the new defaults for the Cray-provided libraries and tools; and
setting 2M hugepages as the default page size ([more info
here](../../../../jobs/best-practices.md#hugepages)). In addition, the
old
[CDT/18.03](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.03_March_1_2018/resource/Released_Cray_XC_Programming_Environments_18.03_March_1_2018.pdf),
[CDT/18.09](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.09_September_6_2018/resource/Released_Cray_XC_Programming_Environments_18.09_September_6_2018.pdf),
and
[CDT/18.12](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_18.12_December_13_2018/resource/Released_Cray_XC_Programming_Environments_18.12_December_13_2018.pdf)
will be removed, and a new release version,
[CDT/19.06](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_19.06_June_20_2019/resource/Released_Cray_XC_(x86)_Programming_Environments_19.06_June_20_2019.pdf),
will be installed.

Below is the detailed list of changes starts on July 30, 2019.

## Software default version changes

* cce/8.7.9 (from 8.7.4)
* cray-fftw/3.3.8.2 (from 3.3.8.1)
* cray-ga/5.3.0.10 (from 5.3.0.8)
* cray-libsci/19.02.1 (from 18.07.1)
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.6 (from 7.7.3)
* cray-parallel-netcdf/1.8.1.4 (from 1.8.1.3)
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.9.3.0 (from 3.8.4.0)
* cray-python/2.7.15.6 (from 2.7.15.1)
* craype/2.5.18 (from 2.5.15)
* gcc/8.2.0 (from 7.3.0)
* modules/3.2.11.1 (from 3.2.10.6)
* papi/5.6.0.6 (from 5.6.0.3)
* perftools, perftools-base, perftools-lite/7.0.6 (from 7.0.3)
* stat/3.0.1.3 (from 3.0.1.2)
* valgrind4hpc/1.0.1 (from N/A)

## Software versions to be removed

* atp/2.1.1
* cce/8.6.5, 8.7.4, 8.7.7
* cray-R/3.3.3
* cray-ccdb/3.0.3
* cray-cti/1.0.6
* cray-fftw/3.3.6.3, 3.3.8.1
* cray-ga/5.3.0.7, 5.3.0.8, 5.3.0.9
* cray-hdf5, cray-hdf5-parallel/1.10.1.1
* lgdb/3.0.7
* cray-libsci/18.03.1, 18.07.1, 18.12.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.0, 7.7.3, 7.7.4
* cray-netcdf, cray-netcdf-hdf5parallel/4.4.1.1.6
* cray-parallel-netcdf/1.8.1.3
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.7.6.2, 3.8.4.0
* cray-python/2.7.13.1, 2.7.15.1
* cray-tpsl, cray-tpsl-64/17.11.1
* cray-trilinos/12.10.1.2
* craype/2.5.14, 2.5.15, 2.5.16
* craypkg-gen/1.3.6
* gcc/7.3.0
* modules/3.2.10.6
* papi/5.5.1.4, 5.6.0.3, 5.6.0.5
* perftools, perftools-base, perftools-lite/7.0.0, 7.0.3, 7.0.5
* pmi, pmi-lib/5.0.13
* stat/3.0.1.1, 3.0.1.2

## New software versions available

* cce/9.0.0
* cray-fftw/3.3.8.3 
* cray-hdf5, cray-hdf5-parallel/1.10.5.0  
* cray-jemalloc/5.1.0.1   
* cray-libsci/19.06.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.8
* cray-netcdf, cray-netcdf-hdf5parallel/4.6.3.0
* cray-parallel-netcdf/1.11.1.0
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.11.2.0
* cray-python/2.7.15.7
* cray-tpsl, cray-tpsl-64	19.06.1
* cray-trilinos/12.14.1.0
* craype/2.6.0
* craype-dl-plugun-py2, craype-dl-plugin-py3/19.06.1
* gcc/8.3.0
* modules/3.2.11.2
* papi/5.7.0.1
* perftools, perftools-base, perftools-lite/7.1.0
